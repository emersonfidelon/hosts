<?php
require 'vendor/autoload.php';
\Slim\Slim::registerAutoloader();
require 'My/View.php';

$app = new \Slim\Slim(array(
    'debug' => true,
    'view' => new View()
));

// After instantiation
$view = $app->view();
$view->setTemplatesDirectory('./templates');
