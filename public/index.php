<?php
require "../bootstrap.php";

/**
* Renderiza a página inicial da aplicação
*/
$app->get('/home', function() use($app) {
    $app->render('principal/index.php', [
        'title' => "Esta é a primeira página enfim renderizada",
    ]);
});

$app->run();